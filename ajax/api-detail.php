<?php 

if(isset($_GET['title'])) {
  $title = $_GET['title'];
}

?>

<script type="text/javascript">



  // Create a request variable and assign a new XMLHttpRequest object to it.
  var request = new XMLHttpRequest();

  var title = "<?php echo $title ?>";

    // Open a new connection, using the GET request on the URL endpoint
    request.open('GET', 'https://chicken-coop.fr/rest/games/'+title, true);

    //alert(title);

    request.onload = function () {
      // Begin accessing JSON data here
      var responseData = JSON.parse(this.response);
      var data = responseData.result;

      if (request.status >= 200 && request.status < 400) {



          // loop response
          // data.forEach(result => {
          // Log each result's title
          // console.log(data.title);
          // console.log(data.releaseDate);
          // console.log(data.image);



          var table='<table class="table table-bordered">';
          table+='<tr><th colspan="2"><img src="'+data.image+'" /></th></tr>';
          table+='<tr><td>Title</td><td>'+data.title+'</td></tr>';
          table+='<tr><td>Release Date</td><td>'+data.releaseDate+'</td></tr>';
          table+='<tr><td>Score</td><td>'+data.score+'</td></tr>';
          table+='<tr><td>Developer</td><td>'+data.developer+'</td></tr>';
          table+='<tr><td>Publisher</td><td>'+data.publisher+'</td></tr>';


        // });

          table+="</table>";

        //Showing the table inside table
        document.getElementById("mydiv").innerHTML = table;
      } else {
        console.log('error');
      }

    }

  // Send request
  request.send();


</script>

<div id="mydiv"></div>
<!-- <a href="http://localhost/smartadmin/#ajax/flot.php"> test </a> -->